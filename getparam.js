const fs = require('fs');
const path = require('path');
const data = require('./data');
let paramConnectString = '';

function getParamObj([keyname, type, text, is_required]){
    let param = '';
    if(is_required){
        param = `    "${keyname}" : {
        "type" : "${type}",
        "text" : "${text}",
        "required" : ${is_required}
    },\n`;
    }
    else{
        param = `   "${keyname}" : {
        "type" : "${type}",
        "text" : "${text}"
    },\n`;  
    }
    return param;
}


data.forEach( (item) => {
    paramConnectString += getParamObj(item);
});

const finalData = `{\n${paramConnectString.endsWith(',\n') ? paramConnectString.slice(0, paramConnectString.length - 2) : paramConnectString}\n}`;
// console.log(finalData);

const filePath = path.join(__dirname, 'param.txt');

fs.writeFile(filePath, finalData, (err) => {
    if (err) throw err;
    console.log(`Check File ==> ${filePath}`);
  });
